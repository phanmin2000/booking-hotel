@extends('client.share.master')
@push('css')
    <link rel="stylesheet" href="{{asset('client/css/index.css')}}" />
@endpush
@section('content')
    <main class="rooms section">
        <div class="container">
            <ul class="rooms_list">
                @foreach($data as $key => $value)
                    <li class="rooms_list-item" data-order="{{$key+1}}" data-aos="fade-up"
                        style="margin-top:20px;width:100%">
                        <div class="item-wrapper d-md-flex">
                            <div class="media">
                                <picture>
                                    <source data-srcset="{{$value->hinh_anh}}" srcset="{{$value->hinh_anh}}"/>
                                    <img style="object-fit: cover;width:420px;height: 280px" class="lazy"
                                         data-src="{{$value->hinh_anh}}" src="{{$value->hinh_anh}}" alt="media"/>
                                </picture>
                            </div>
                            <div class="main d-md-flex justify-content-between" style="flex: 1;">
                                <div class="main_info d-md-flex flex-column justify-content-between">
                                    <a class="main_title h4" href="room.html">Loại phòng {{$value->ma_phong}} <span
                                            style="font-size:16px">({{$value->countRoom}} phòng)</span></a>
                                    <div class="main_amenities">
                                        <span class="main_amenities-item d-inline-flex align-items-center">
                                            <i class="icon-user icon"></i>
                                            {{$value->so_khach}} người / phòng
                                        </span>
                                        <br>
                                        <span class="main_amenities-item d-inline-flex align-items-center">
                                            <i class="icon-bunk_bed icon"></i>
                                            {{$value->view}}
                                        </span>
                                    </div>
                                </div>
                                <div style="margin-left:auto"
                                     class="main_pricing d-flex flex-column align-items-md-end justify-content-md-between">
                                    <div class="wrapper d-flex flex-column">
                                        <span class="main_pricing-item">
                                        <span class="h4">{{ number_format($value->gia_mac_dinh, 0, ',', '.') }}đ</span>
                                            / 1 đêm
                                        </span>
                                    </div>
                                    <a class="theme-element theme-element--accent btn"
                                       href="{{route('detail-room',$value->id)}}">Đặt phòng ngay</a>
                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach

            </ul>
            <nav>
                <ul class="pagination pagination-rounded mb-0">
                    {{ $data->links() }}
                </ul>
            </nav>
            {{--            <ul class="pagination d-flex align-items-center">--}}
            {{--                <li class="pagination-page">--}}
            {{--                    <a class="pagination-page_link d-flex align-items-center justify-content-center" href="#" data-current="true">1</a>--}}
            {{--                </li>--}}
            {{--                <li class="pagination-page">--}}
            {{--                    <a class="pagination-page_link d-flex align-items-center justify-content-center" href="#">2</a>--}}
            {{--                </li>--}}
            {{--                <li class="pagination-page">--}}
            {{--                    <a class="pagination-page_link d-flex align-items-center justify-content-center" href="#">3</a>--}}
            {{--                </li>--}}
            {{--            </ul>--}}
        </div>
    </main>

@endsection
@section('js')
    <script src="{{asset('client/js/index.min.js')}}"></script>
@endsection
